# Caraya Unit Test Framework CLI Operation
This project is a LabVIEW Class that will run as Custom Operation using the NI LabVIEW CLI application.\
It is called Caraya Unit Tests and it generates as results a JUnit report or in a text format if preferred.

# Author
Felipe Pinheiro Silva

## Installation

To install you may use the NI Package Manager available in the [releases page](../../releases).\
Additionally, it is possible use from the [docker images](https://hub.docker.com/r/felipefoz/vianalyzer) in Docker Hub.

## Dependencies
### Packages
- VIPM [Caraya Unit Test Framework](https://www.vipm.io/package/jki_lib_caraya/).

### Testing
This project uses Caraya Unit Test Framework and the VI Analyzer Toolkit

## Usage
Operation Name - **CarayaUnitTest**.\
Runs tests on the specified files in the Caraya Unit Test Framework Toolkit and saves the JUnit (Text) file to the specified location.\
The following table lists the available arguments of this operation. Required arguments are in **bold**.

| Argument | Description | Default |
| :-------- | :----------- | :------- |
| **-ProjectPath** | 	Path to the project root directory or the folder that contains the Unit Tests | - |
| **−ReportPath** |	Path to the report/results file. |	- |
| −ReportSaveType	| Format of the report or results file. The value for this argument must be one of the following: JUNIT (XML) or Text (ASCII) | JUnit |

`LabVIEWCLI -OperationName CarayaUnitTest -ProjectPath <path of LabVIEW project> -ReportPath <name of the JUnit report path> [-ReportSaveType <format of the report>]>`

## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.\
Please make sure to update tests as appropriate.

## License
[BSD3](LICENSE)
